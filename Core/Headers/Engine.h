#pragma once

#include "Singleton.h"
#include "EngineOptions.h"

namespace tin {
	class Engine : public Singleton<Engine> {
	public:
		void SetOptions(EngineOptions options);
	private:
		virtual void Init();
		virtual void Shutdown();

		EngineOptions options_;

		Engine();
		~Engine();

		friend class Singleton<Engine>;
	};
}


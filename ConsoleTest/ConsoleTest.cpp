// ConsoleTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Engine.h>
#include <TaskManager.h>
#include <functional>
#include <iostream>

class Tester{
public:
	Tester() {
		
	}

	void Test() {
		std::cout << "Tester::Test()" << std::endl;
	}

	void print() {
		std::cout << "Tester" << std::endl;
	}
};
void Test() {
	std::cout << "Test" << std::endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	tin::Engine::Create();
	tin::Engine::Destroy();
	
	Tester *tester = new Tester();


	tin::Task task(&Tester::Test, tester);
	tin::Task task1(&Tester::Test, tester);
	tin::Task task2(&Tester::Test, tester);

	task2.SetPriority(1);
	task.SetPriority(3);
	tin::TaskManager::Create();
	tin::TaskManager::SharedInstance()->AddTask(task);
	tin::TaskManager::SharedInstance()->AddTask(task1);
	tin::TaskManager::SharedInstance()->AddTask(task2);

	delete tester;
	
	tin::TaskManager::Destroy();
	getchar();
	return 0;
}


//
//  Logger.h
//  Engine Model A
//
//  Created by Tobias Boogh on 3/19/12.
//  Copyright (c) 2012 t.inc.an. All rights reserved.
//

#ifndef _Logger_H_
#define _Logger_H_


#include <list>
#include <mutex>
#include "Singleton.h"
namespace tin{  
    class LoggerInterface;
    class LoggingSystem : public Singleton<LoggingSystem>{
        public:

            static void LogMessage(const char *format, ...);
            static void LogWarning(const char *format, ...);
            static void LogError(const char *format, ...);
                
            void AddInterface(LoggerInterface *interface);
            void RemoveInterface(LoggerInterface *interface);
    
        private:
            void Init();
            void Shutdown();
            ~LoggingSystem();
            LoggingSystem();
        
            void LogMessageInternal(const char *message);
            void LogWarningInternal(const char *message);
            void LogErrorInternal(const char *message);
        
            std::list<LoggerInterface *>    interfaces_;
            
            std::mutex logging_mutex_;
            friend class Singleton<LoggingSystem>;
    };
} // tin
#endif

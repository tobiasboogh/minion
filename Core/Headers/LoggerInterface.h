//
//  LoggerInterface.h
//  Engine Model A
//
//  Created by Tobias Boogh on 3/19/12.
//  Copyright (c) 2012 t.inc.an. All rights reserved.
//

#ifndef _LoggerInterface_H_
#define _LoggerInterface_H_

#define Bit(X)(1 << X)
typedef int LoggerOption;

typedef enum{
    ALL     = Bit(0),
    ERROR   = Bit(1),
    WARNING = Bit(2),
    MESSAGE = Bit(3),
    VERBOSE = Bit(4)
} LoggerOptions;

namespace tin{
    class LoggerInterface {
        public:
            LoggerInterface() { };
            virtual ~LoggerInterface() { };
        
            virtual void LogMessage(const char *message) = 0;
            virtual void LogWarning(const char *message) = 0;
            virtual void LogError(const char *message) = 0;
        
            virtual LoggerOption RespondsToOptions() = 0;
    };
} // tin
#endif

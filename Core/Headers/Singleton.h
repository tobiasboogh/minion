//
//  Singleton.h
//  Engine
//
//  Created by Tobias Boogh on 4/1/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#ifndef _Singleton_H_
#define _Singleton_H_
namespace tin{
    template <typename T>
    class Singleton{
        public:
            static void Create(){
                if (instance_ == nullptr){
                    instance_ = new T();
                    instance_->Init();
                }
            }
        
            static void Destroy(){
                if (instance_ != nullptr){
                    instance_->Shutdown();
                    delete instance_;
                    instance_ = nullptr;
                }
            }
        
            static T* SharedInstance(){
                return instance_;
            }
        private:
            static T *instance_;
            virtual void Init() = 0;
            virtual void Shutdown() = 0;
    };
    
    template<typename T>
    T* Singleton<T>::instance_ = nullptr;
} // tin
#endif

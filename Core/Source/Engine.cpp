#include "../Headers/Engine.h"
#include "../Headers/Profiler.h"

namespace tin {
	Engine::Engine() {
		
	}

	Engine::~Engine() {}

	void Engine::Init() {
		Profiler::Create();
	}

	void Engine::Shutdown() {
		Profiler::Destroy();
	}

	void Engine::SetOptions(EngineOptions options) {
		options_ = options;
	}
}
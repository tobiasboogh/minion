#pragma once
#include "Singleton.h"
#include <string>
namespace tin {
	struct ProfileInfo {
		std::string detail;
	};

	class Profiler : public Singleton<Profiler> {
	public:
		long Start(std::string name);
		void Finish(long identifier);
	private:
		virtual void Init();
		virtual void Shutdown();

		Profiler();
		~Profiler();

		long task_identifier_;

		friend class Singleton<Profiler>;
	};
}

